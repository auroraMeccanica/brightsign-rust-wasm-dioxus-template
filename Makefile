release:
	rm -rf generated
	cd rust_app;dx build --release;
	cd nodejs_server; make release;
	mkdir -p generated/public
	cp -r rust_app/dist/* generated/public/
	cp nodejs_server/dist/bundle.js generated/

install-dependencies:
	cargo install dioxus-cli
	cd nodejs_server; make install-dependencies;

clean:
	rm -rf generated
	cd nodejs_server;rm -rf dist; rm -rf node_modules;
	cd rust_app; cargo clean; rm -rf dist

.PHONY=clean release install-dependencies
