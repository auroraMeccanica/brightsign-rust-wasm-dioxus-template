# Template brightsign for Rust+Dioxus

This templates serves as a quickstart to publish webapps built in Rust with the [dioxus library](https://dioxuslabs.com/) on a Brightsign player.

The project can run on the brightsign without an internet connection. as long as you include all resources as static files or by bundling them in the binary.

You can build and prepare the entire project for publishing easily by using the provided `Makefile`.

The template is constituted of two subfolders:
- `rust_app` contains a barebone rust program, showing how to include the stylesheet inside the resulting binary; this uses the `dioxus_web` renderer, and it is built to a wasm file target;
- `nodejs_server` contains the nodeJs static file server that will be run locally inside the brightsign player unit.

# Prerequisites

These instructions apply to a GNU/Linux operating system, but you might be able to make it work on a Windows OS too with additional steps (not tested).

You need:
- access to a UNIX shell;
- `git` command;
- `make` command is available;
- rust compiler and `cargo` tools installed, configured with a wasm target `wasm32-unknown-unknown`: if not installed, run `rustup target add wasm32-unknown-unknown`;
- nodeJs's `npm` and `npx` commands should be installed.

First, initialize the submodules and download them.
```
git submodule init
git submodule update
```

Then, open a terminal inside this folder location and run
```bash
make install-dependencies
```
This command will install the `dx` tool to build dioxus projects and automatically download all the nodejs project dependencies needed.



# Publishing

The nodejs server is configured to serve files at the `public/` folder inside the sd card root at `http://localhost:9090/`.


## Building files

Run this command to build both the rust project, both assembling the `bundle.js` file with webpack.
```sh
make release
```

Output files will be put inside the newly created `generated` directory.


## Uploading to brightsign

Have a look at the official brightsign nodejs test repository <https://github.com/brightsign/node.js-starter-project>.

Basically, you have to create a new presentation, add a HTML content set at URL `http://localhost:9090` and attach the generated `bundle.js` in "Supported content → NodeJS" section.
Later, copy the whole `public` folder, found inside the `generated directory`, to the root of the sd card you will put into the brightsign player.
