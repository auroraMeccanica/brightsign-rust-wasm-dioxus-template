#![allow(non_snake_case)]
// import the prelude to get access to the `rsx!` macro and the `Scope` and `Element` types
use dioxus::prelude::*;

fn main() {
    // launch the web app
    dioxus_web::launch_cfg(App, dioxus_web::Config::new());
}

// create a component that renders a div with the text "Hello, world!"
fn App(cx: Scope) -> Element {
    cx.render(rsx! {
        style
        {
            // dangerous_inner_html: include_str!("mini-default.min.css")
            dangerous_inner_html: include_str!("simple.min.css")
        }
        div {
            class:"container",
            h1 {"Hello, world"}
            p {"Lorem ipsum qualcosa"}
            hr {}
            button {
                r#type:"button",
                class:"primary",
                "ASDSADDAS"
            }
            div
            {
                class:"card",
                h2 {"titolo"}
                p {"SDADSSDSDn asd asdjk asdjksad"}
            }
        }
    })
}
